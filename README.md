# try-capsule

## Install Capsule

### Linux
```bash
export CAPSULE_VERSION="v0.3.0"
wget -O - https://raw.githubusercontent.com/bots-garden/capsule/${CAPSULE_VERSION}/install-capsule-launcher.sh| bash
```

### MacOS
```bash
mkdir $HOME/.local/bin
export CAPSULE_RUNNER_PATH="$HOME/.local"
export PATH="$CAPSULE_RUNNER_PATH/bin:$PATH"

export CAPSULE_VERSION="v0.3.0"
wget -O - https://raw.githubusercontent.com/bots-garden/capsule/${CAPSULE_VERSION}/install-capsule-launcher.sh| bash
```

## Serve a wasm module

```bash
MESSAGE="👋 Hello World 🌍" capsule \
  -wasm=./app/index.wasm \
  -mode=http \
  -httpPort=8080
```

## Start NATS server

```bash

```