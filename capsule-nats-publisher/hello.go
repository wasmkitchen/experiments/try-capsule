package main

import (
	hf "github.com/bots-garden/capsule/capsulemodule/hostfunctions"
)

func main() {
	hf.SetHandle(Handle)
}

func Handle(params []string) (string, error) {

	// a new connection is created at every call/publish,
	// publish a message to the "ping" subject
	_, err := hf.NatsConnectPublish(
		"localhost:4222",
		"ping",
		"🖐 Hello from WASM with Nats 💜")

	if err != nil {
		hf.Log(err.Error())
	}

	return "NATS Rocks!", err
}
