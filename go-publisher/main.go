package main

import (
	"fmt"
	"github.com/nats-io/nats.go"
)

func main() {
	// Connect to a server
	nc, err := nats.Connect("localhost:4222")
	if err != nil {
		fmt.Println(err.Error())
	}
	defer nc.Close()

	err = nc.Publish("ping", []byte("😍 Hello World"))

	err = nc.Publish("ping", []byte("😁😁 Hello World"))

	if err != nil {
		fmt.Println(err.Error())
	}

}
